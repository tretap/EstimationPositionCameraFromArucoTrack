#This Programs operation on Python 3.x ++ 
#Auther: Treethep but.. math theory from Prof.Pi Thanacha
#This programs create for project Robotic in Fibo for locolization with Aruco track 

import time
import cv2
import cv2.aruco as aruco
from picamera.array import PiRGBArray
from picamera import PiCamera

import numpy as np

#This Function Return Position in Gobal Frame
def estimationPoseCamera(_dG, _R, _dR):
    _sum = np.matmul(_R,_dR)
    return np.matrix.transpose(_dG) - _sum

def estimationPoseCameraInMultiTrack(idz, _dG, _R, _dR):
    idz = idz[0]
    pG = estimationPoseCamera(_dG[str(idz)], _R, _dR)

    return pG

if __name__ == "__main__":
    
    camera = PiCamera()
    camera.resolution = (640, 480)
    camera.framerate = 32
    rawCapture = PiRGBArray(camera, size=(640, 480))

    time.sleep(0.1)

    dG = []
    dG.append(np.matrix([0.0, 0.0, 0.0]))

    pGlist = []
    pGdict =	{
        "50": [],
        "56": [],
        "66": []
    }

    originPGdict = pGdict 

    dG = {
        "50": np.matrix([0.0, 0.0, 0.0]),
        "56": np.matrix([0.0, 0.0, 0.0]),
        "66": np.matrix([0.0, 0.0, 0.0])
    }

    with np.load('B.npz') as X:
        mtx, dist, _, _ = [X[i] for i in ('mtx','dist','rvecs','tvecs')]

    for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
        # Capture frame-by-frame
        frame = frame.array

        # Our operations on the frame come here
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
        parameters =  aruco.DetectorParameters_create()

        corners, ids, rejectedImgPoints = aruco.detectMarkers(frame, aruco_dict,parameters=parameters)
        
        pGlist = []
        pGdict = originPGdict

        if(corners != []):
            rentVar = [0.0, 0.0, 0.0]

            for i in range(len(ids)):
                c = [np.asarray(corners[i], dtype=np.float32)]
                frame = aruco.drawDetectedMarkers(frame, [np.asarray(corners[i], dtype=np.float32)],ids[i])
                rvecs, tvecs, _ = aruco.estimatePoseSingleMarkers(c, 0.056, mtx, dist) #<-- Set size of Maker, Unit in meter.
                r = rvecs
                t = tvecs
                
                RM ,JB = cv2.Rodrigues(rvecs) #RM = Rotation Matrix, JB eqeaul Jacobian

                aruco.drawAxis(frame, mtx, dist, r,t, 0.05)

                #------Old Command-------#
                #pG = estimationPoseCamera(dG[0], np.matrix(RM), np.matrix.transpose(np.matrix(t)))
                
                pG = estimationPoseCameraInMultiTrack(ids[i], dG, np.matrix(RM), np.matrix.transpose(np.matrix(t)))
                
                #----------Display Position in way of TEXT!---------#
                #print(pG)
                pGdict[str(ids[i][0])] = pG
                
                for j in range(3):
                    rentVar[j] += (float(pG[j])/float(ids[i][0]))


            finalValue = rentVar

            #----------Display Position in way of TEXT!---------#
            #print(pGdict)
            print(finalValue) #I Think this unit in meter too.

        # Display the resulting frame
        cv2.imshow('frame',frame)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cv2.destroyAllWindows()

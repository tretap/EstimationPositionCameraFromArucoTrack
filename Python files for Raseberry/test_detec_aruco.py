import time
import cv2
import cv2.aruco as aruco
from picamera.array import PiRGBArray
from picamera import PiCamera

import numpy as np

camera = PiCamera()
camera.resolution = (640, 480)
camera.framerate = 32
rawCapture = PiRGBArray(camera, size=(640, 480))

time.sleep(0.1)

dG = []
#dG.append(np.matrix([0.254, 0.158, 0.215])) # x, y, z
dG.append(np.matrix([0.0, 0.0, 0.0]))

#This Function Return Position in Gobal Frame
def estimationPoseCamera(_dG, _R, _dR):
    #r = np.ndarray.tolist(_R)
    #_R = np.matrix([[r[0][0],r[0][1],0], [r[1][0],r[1][1],0], [0,0,1]])    
    #print(np.ndarray.tolist(_R)[2][0])
    #                       print(_R)

    _sum = np.matmul(_R,_dR)
    #print('_sum: ' + str(_sum))
    #print('_dG: ' + str(np.matrix.transpose(_dG)))
    return np.matrix.transpose(_dG) - _sum
 

with np.load('B.npz') as X:
    mtx, dist, _, _ = [X[i] for i in ('mtx','dist','rvecs','tvecs')]

for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
    # Capture frame-by-frame
    frame = frame.array
    #print(frame.shape) #480x640
    # Our operations on the frame come here
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
    parameters =  aruco.DetectorParameters_create()

    corners, ids, rejectedImgPoints = aruco.detectMarkers(frame, aruco_dict,parameters=parameters)
    #print(corners)
    #print(ids)

    if(corners != []):
        c = corners
        frame = aruco.drawDetectedMarkers(frame, corners,ids)
        rvecs, tvecs, _ = aruco.estimatePoseSingleMarkers(c, 0.056, mtx, dist)
        r = rvecs
        t = tvecs
        
        #print(t)
        RM ,JB = cv2.Rodrigues(rvecs) #RM = Rotation Matrix, JB eqeaul Jacobian

        #print(str(len(RM)) +':' + str(len(RM[0])))
        #print('Rotation: ' + str(RM))
        #print('Translation: ' +str(t))

        aruco.drawAxis(frame, mtx, dist, r,t, 0.05);

        #print('Matrix: ' + str(np.matrix(RM)))
        #print('Matrix Transpose: ' + str(np.matrix.transpose(np.matrix(RM))))
        pG = estimationPoseCamera(dG[0], np.matrix(RM), np.matrix.transpose(np.matrix(t)))
        print(pG)

    
    #print(rejectedImgPoints)
    # Display the resulting frame
    #cv2.undistort(frame, mtx, dist, None, mtx)
    cv2.imshow('frame',frame)
    rawCapture.truncate(0)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cv2.destroyAllWindows()
